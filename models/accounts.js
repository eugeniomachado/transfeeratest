'use strict';
module.exports = (sequelize, DataTypes) => {
  const Accounts = sequelize.define('Accounts', {
    agency: DataTypes.INTEGER,
    accountNumber: DataTypes.INTEGER,
    accountType: DataTypes.STRING,
    digitAcc: DataTypes.INTEGER,
    digitAg: DataTypes.STRING,
    bankId: DataTypes.INTEGER,
  }, {});
  Accounts.associate = (models) => {
    Accounts.hasMany(models.Banks, {foreignKey: 'id'})
  };
  return Accounts;
};
