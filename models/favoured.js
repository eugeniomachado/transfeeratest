

'use strict';
module.exports = (sequelize, DataTypes) => {
  const Favoured = sequelize.define('Favoured', {
    name: DataTypes.STRING,
    identification: DataTypes.STRING,
    email: DataTypes.STRING,
    status: DataTypes.ENUM('VALID', 'DRAFT'),
    accountId: DataTypes.NUMBER
  }, {});
  Favoured.associate = (models) => {
    Favoured.hasOne(models.Accounts, {
      foreignKey: 'id',
      onDelete: 'cascade'
    })
  };
  return Favoured;
};