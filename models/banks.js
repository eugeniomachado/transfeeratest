'use strict';
module.exports = (sequelize, DataTypes) => {
  const Banks = sequelize.define('Banks', {
    name: DataTypes.STRING,
    code: DataTypes.STRING
  }, {});
  return Banks;
};