var express = require("express");
var router = express.Router();
var db = require("../models/index");

router.get("/", function(req, res, next) {
  db.Accounts.findAll({  include: [
    {
      model: db.Banks
    }
  ]})
    .then(Accounts =>
      res.json({
        error: false,
        data: Accounts
      })
    )
    .catch(error =>
      res.json({
        error: true,
        data: [],
        error: error
      })
    );
});

router.post("/", async function(req, res, next) {
  const { name, code } = req.body;

  db.Accounts.create({
    name: name,
    code: code,
    bankId: bankId
  })
    .then(account =>
      res.status(201).json({
        error: false,
        data: account,
        message: "account has been created."
      })
    )
    .catch(error =>
      res.json({
        error: true,
        data: [],
        error: error
      })
    );
});

router.put("/:id", function(req, res, next) {
  const { name, code, bankId } = req.body;

  db.Accounts.update(
    {
      name: name,
      code: code,
      bankId: bankId
    },
    {
      where: {
        id: req.params.id
      }
    }
  )
    .then(account =>
      res.status(201).json({
        error: false,
        data: account,
        message: "account has been updated."
      })
    )
    .catch(error =>
      res.json({
        error: true,
        data: [],
        error: error
      })
    );
});

router.delete("/:id", function(req, res, next) {
  db.Accounts.destroy({
    where: {
      id: req.params.id
    }
  })
    .then(status =>
      res.json({
        error: false,
        message: "account has been deleted."
      })
    )
    .catch(error =>
      res.json({
        error: true,
        error: error
      })
    );
});

module.exports = router;
