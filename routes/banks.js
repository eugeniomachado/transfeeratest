var express = require("express");
var router = express.Router();
var db = require("../models/index");

router.get("/", function(req, res, next) {
  db.Banks.findAll({})
    .then(banks =>
      res.json({
        error: false,
        data: banks
      })
    )
    .catch(error =>
      res.json({
        error: true,
        data: [],
        error: error
      })
    );
});

router.post("/", async function(req, res, next) {
  const { name, code } = req.body;

  db.Banks.create({
    name: name,
    code: code
  })
    .then(bank =>
      res.status(201).json({
        error: false,
        data: bank,
        message: "Bank has been created."
      })
    )
    .catch(error =>
      res.json({
        error: true,
        data: [],
        error: error
      })
    );
});

router.put("/:id", function(req, res, next) {
  const { name, code } = req.body;

  db.Banks.update(
    {
      name: name,
      code: code
    },
    {
      where: {
        id: req.params.id
      }
    }
  )
    .then(bank =>
      res.status(201).json({
        error: false,
        data: bank,
        message: "Bank has been updated."
      })
    )
    .catch(error =>
      res.json({
        error: true,
        data: [],
        error: error
      })
    );
});

router.delete("/:id", function(req, res, next) {
  db.Banks.destroy({
    where: {
      id: req.params.id
    }
  })
    .then(status =>
      res.json({
        error: false,
        message: "Bank has been deleted."
      })
    )
    .catch(error =>
      res.json({
        error: true,
        error: error
      })
    );
});

module.exports = router;
