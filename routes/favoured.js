var express = require("express");
var router = express.Router();
var db = require("../models/index");

router.get("/", function (req, res, next) {
  db.Favoured.findAll({ include: [{ model: db.Accounts }] })
    .then(favoured =>
      res.json({
        error: false,
        data: favoured
      })
    )
    .catch(error =>
      res.json({
        error: true,
        data: [],
        error: error
      })
    );
});

router.get("/:id", function (req, res, next) {
  db.Favoured.findOne({
    where: {
      id: req.params.id
    },
    include: [{ model: db.Account }]
  })
    .then(favoured =>
      res.status(201).json({
        error: false,
        data: favoured
      })
    )
    .catch(error =>
      res.json({
        error: true,
        data: [],
        error: error
      })
    );
});

router.post("/", async function (req, res, next) {
  db.Accounts.create(req.body.Account).then(acc => {
    const favoured = req.body;
    favoured.accountId = acc.dataValues.id;
    db.Favoured.create(favoured)
      .then(bank =>
        res.status(201).json({
          error: false,
          data: bank,
          message: "Favoured has been created."
        })
      )
      .catch(error =>
        res.json({
          error: true,
          data: [],
          error: error
        })
      );
  });
});

router.put("/", function (req, res, next) {
  db.Accounts.update(req.body.Account, {
    where: {
      id: req.body.accountId
    }
  }).then(
    db.Favoured.update(req.body, {
      where: {
        id: req.body.id
      }
    })
      .then(favoured =>
        res.status(201).json({
          error: false,
          data: favoured,
          message: "Favoured has been updated."
        })
      )
      .catch(error =>
        res.json({
          error: true,
          data: [],
          error: error
        })
      )
  );
});

router.delete("/:id/:accountId", function (req, res, next) {

    db.Favoured.destroy({
      where: {
        id: req.params.id
      }
    })
    .then(status =>
      db.Accounts.destroy({
        where: {
          id: req.params.accountId
        }
      }),
      res.json({
        error: false,
        message: "Favoured has been deleted."
      })
    )
    .catch(error =>
      res.json({
        error: true,
        error: error
      })
    );
});

module.exports = router;
