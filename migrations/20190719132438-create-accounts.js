'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Accounts', {
      bankId: {
        type: Sequelize.INTEGER,
        allowNull: false,
        references: {
          model: "Banks",
          key: "id"
        }
      },
      agency: {
        type: Sequelize.INTEGER
      },
      accountType: {
        type: Sequelize.STRING
      },
      accountNumber: {
        type: Sequelize.BIGINT
      },
      digitAcc: {
        type: Sequelize.INTEGER
      },
      digitAg: {
        type: Sequelize.STRING
      },
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    })
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Accounts');
  }
};
